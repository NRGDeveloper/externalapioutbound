﻿using Microsoft.EntityFrameworkCore;

namespace ExternalAPIOutbound.Data
{
    public class ExternalAPIOutboundContext : DbContext
    {
        public ExternalAPIOutboundContext (DbContextOptions<ExternalAPIOutboundContext> options)
            : base(options)
        {
        }

        public DbSet<ExternalAPIOutbound.Models.auditAPICalls> auditAPICalls { get; set; }
    }
}
