﻿namespace ExternalAPIOutbound
{
    public class FFAuthResponse
    {
        public string authToken { get; set; }
        public string timeout { get; set; }
        public string version { get; set; }
    }
}
