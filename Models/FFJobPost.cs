﻿namespace ExternalAPIOutbound
{
    public class FFJobPost
    {
        public string authCode { get; set; }
        public string jobNumber { get; set; }
        public string fleetNumber { get; set; }
        public string registration { get; set; }
        public string dateIn { get; set; }
        public string jobStatus { get; set; }
        public string workshopCode { get; set; }
    }
}
