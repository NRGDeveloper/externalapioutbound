﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ExternalAPIOutbound.Models
{
    public class auditAPICalls
    {
        [Key]
        public int id { get; set; }
        [StringLength(200)]
        public string methodName { get; set; }
        [StringLength(200)]
        public string authToken { get; set; }
        [StringLength(200)]
        public string paramNames { get; set; }
        [StringLength(500)]
        public string paramValues { get; set; }
        [StringLength(2000)]
        public string ipAddresses { get; set; }

        public DateTime dtTime { get; set; }

    }
}
