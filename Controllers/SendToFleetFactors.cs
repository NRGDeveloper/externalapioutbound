﻿using ExternalAPIOutbound.Data;
using ExternalAPIOutbound.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ExternalAPIOutbound.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SendToFleetFactors : ControllerBase
    {
        private readonly ExternalAPIOutboundContext _context;

        public SendToFleetFactors(ExternalAPIOutboundContext context)
        {
            _context = context;
        }

        // GET api/<SendToFleetFactors>/5
        [HttpGet("{id}")]
        public async Task<string> GetAsync(int id, string fleetNumber, string registration, string workshopCode, string OpenOrClose)
        {
            var hostName = Dns.GetHostName();
            var ips = Dns.GetHostAddresses(hostName).ToList();
            var x = string.Join(',', ips);

            _context.auditAPICalls.Add(new auditAPICalls
            {
                methodName = "Send open job",
                paramNames = "id, fleetNumber, registration, workshopCode, OpenOrClose",
                paramValues = id.ToString() + "," + fleetNumber + "," + registration + "," + workshopCode + "," + OpenOrClose,
                ipAddresses = x,
                dtTime = DateTime.Now
            }); ;
            _context.SaveChanges();

            string status = "Closed";
            if (OpenOrClose == "O")
            {
                status = "In Progress";

            };
            // Authenticate
            //            string uriPath = "https://fleetfactorsecatwebtest.azurewebsites.net/NRGAPI/Authenticate?token=nfaP9NcuU+vGj3WIyYAgDZlIS0rcqlWyEOUfBbRNdjhojgNQIu5IZPLQT4VxS/eq";

            string uriPath = "https://fleetfactorsecatweblive.azurewebsites.net/NRGAPI/Authenticate?token=nfaP9NcuU+vGj3WIyYAgDZlIS0rcqlWyEOUfBbRNdjhojgNQIu5IZPLQT4VxS/eq";

            HttpClient clientAuth = new HttpClient();
            clientAuth.BaseAddress = new Uri(uriPath);
            clientAuth.DefaultRequestHeaders.Accept.Clear();
            clientAuth.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await clientAuth.GetAsync(uriPath);
            FFAuthResponse res = null;

            res = await response.Content.ReadAsAsync<FFAuthResponse>();

            //uriPath = "https://fleetfactorsecatwebtest.azurewebsites.net/nrgapi/Job";

            uriPath = "https://fleetfactorsecatweblive.azurewebsites.net/nrgapi/Job";

            HttpClient clientPost = new HttpClient();
            clientPost.BaseAddress = new Uri(uriPath);
            clientPost.DefaultRequestHeaders.Accept.Clear();
            clientPost.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            FFJobPost jobPost = new FFJobPost
            {
                authCode = res.authToken,
                jobNumber = id.ToString(),
                fleetNumber = fleetNumber,
                registration = registration,
                jobStatus = status,
                workshopCode = workshopCode
            };
            string jsonJobPost = JsonConvert.SerializeObject(jobPost);
            var httpContent = new StringContent(jsonJobPost, Encoding.UTF8, "application/json");

            var httpClient = new HttpClient();
            var url = uriPath;
            var data = jobPost;
            var source = new CancellationTokenSource();

            var resp = await httpClient.PostAsJsonAsync<FFJobPost>(url, data, source.Token);

            return "1";
        }

    }
}
