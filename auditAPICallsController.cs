﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ExternalAPIOutbound.Data;
using ExternalAPIOutbound.Models;

namespace ExternalAPIOutbound
{
    [Route("api/[controller]")]
    [ApiController]
    public class auditAPICallsController : ControllerBase
    {
        private readonly ExternalAPIOutboundContext _context;

        public auditAPICallsController(ExternalAPIOutboundContext context)
        {
            _context = context;
        }

        // GET: api/auditAPICalls
        [HttpGet]
        public async Task<ActionResult<IEnumerable<auditAPICalls>>> GetauditAPICalls()
        {
            return await _context.auditAPICalls.ToListAsync();
        }

        // GET: api/auditAPICalls/5
        [HttpGet("{id}")]
        public async Task<ActionResult<auditAPICalls>> GetauditAPICalls(int id)
        {
            var auditAPICalls = await _context.auditAPICalls.FindAsync(id);

            if (auditAPICalls == null)
            {
                return NotFound();
            }

            return auditAPICalls;
        }

        // PUT: api/auditAPICalls/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutauditAPICalls(int id, auditAPICalls auditAPICalls)
        {
            if (id != auditAPICalls.id)
            {
                return BadRequest();
            }

            _context.Entry(auditAPICalls).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!auditAPICallsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/auditAPICalls
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<auditAPICalls>> PostauditAPICalls(auditAPICalls auditAPICalls)
        {
            _context.auditAPICalls.Add(auditAPICalls);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetauditAPICalls", new { id = auditAPICalls.id }, auditAPICalls);
        }

        // DELETE: api/auditAPICalls/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<auditAPICalls>> DeleteauditAPICalls(int id)
        {
            var auditAPICalls = await _context.auditAPICalls.FindAsync(id);
            if (auditAPICalls == null)
            {
                return NotFound();
            }

            _context.auditAPICalls.Remove(auditAPICalls);
            await _context.SaveChangesAsync();

            return auditAPICalls;
        }

        private bool auditAPICallsExists(int id)
        {
            return _context.auditAPICalls.Any(e => e.id == id);
        }
    }
}
